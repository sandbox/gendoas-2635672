<?php

class MyPhpUnitResultPrinter extends PHPUnit_TextUI_ResultPrinter {

  /**
   * @var    array
   */
  protected static $result;

  /**
   * 
   * @return array
   */
  public static function getTestsResult() {
    return self::$result;
  }

  /**
   * Constructor.
   *
   * @param  mixed $out
   * @throws PHPUnit_Framework_Exception
   */
  public function __construct() {
    self::$result = '';
  }

 /**
   * @param  PHPUnit_Framework_TestFailure $defect
   */
  protected function printDefectTrace(PHPUnit_Framework_TestFailure $defect) {
    $buffer = $defect->getExceptionAsString() . "\n" .
            PHPUnit_Util_Filter::getFilteredStacktrace(
                    $defect->thrownException());
    $this->write($this->cleanDefectTrace($buffer));

    $e = $defect->thrownException()->getPrevious();

    while ($e) {
      $this->write(
              "\nCaused by\n" .
              PHPUnit_Framework_TestFailure::exceptionToString($e) . "\n" .
              PHPUnit_Util_Filter::getFilteredStacktrace($e)
      );

      $e = $e->getPrevious();
    }
  }

  /**
   * @param  string $buffer
   */
  public function write($buffer) {
    self::$result .= $buffer;
  }
  
  /**
   * Truncate trace, show until <em>/myphpunit/myphpunit.module:</em> only
   * 
   * @param string $buffer
   * @return string
   */
  private function cleanDefectTrace($buffer) {
    $pattern = '/myphpunit/myphpunit.module:';
    if (strpos($buffer, $pattern) === false) {
      return $buffer;
    }
    
    $lines = explode("\n", $buffer);
    $buffer = [];
    foreach ($lines as $line) {
      $buffer[] = $line;
      if (strpos($line, $pattern) !== false) {
        $buffer[] = '';
        break;
      }
    }
    
    return implode("\n", $buffer);  
  }

}
