<?php

class MyPhpUnitListener extends PHPUnit_Util_Log_JSON {

  /**
   * @var    array
   */
  protected static $result;

  /**
   * 
   * @return array
   */
  public static function getTestsResult() {
    return self::$result;
  }

  /**
   * Constructor.
   *
   * @param  mixed $out
   * @throws PHPUnit_Framework_Exception
   */
  public function __construct() {
    self::$result = [];
  }

  /**
   * @param string $status
   * @param float  $time
   * @param array  $trace
   * @param string $message
   */
  protected function writeCase($status, $time, array $trace = [], $message = '', $test = NULL) {
    if (!empty($trace)) {
      $items = $trace;
      $trace = [];
      foreach ($items as $item) {
        $trace[] = $item;

        if (isset($item['class']) && isset($item['function'])) {
          if ($item['class'] == 'PHPUnit_TextUI_Command' && $item['function'] == 'main') {
            break;
          }
        }
      }
    }

    parent::writeCase($status, $time, $trace, $message, $test);
  }

  /**
   * @param string $buffer
   */
  public function write($buffer) {
    self::$result[] = $buffer;
  }

}
