<?php
/**
 * @file
 * Contains \Drupal\myphpunit\SelfTest.
 */

namespace Drupal\myphpunit;

//use Drupal\myphpunit\MyPhpUnitTestCase;

/**
 * For testing module myphpunit tests
 */
class SelfTest extends MyPhpUnitTestCase {

  public function testFailure() {
    $this->assertTrue(false, 'SelfTest: expected Failure.');
  }
  public function testSuccess() {
    $this->assertTrue(true, 'SelfTest: expected no Failure.');
  }

}
