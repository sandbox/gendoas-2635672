<?php

/**
 * @file
 * Contains Drupal\myphpunit\MyPhpUnitTestCase.
 */

namespace Drupal\myphpunit;

use Drupal\Tests\UnitTestCase;
use Drupal\user\Entity\User;
use Drupal\user\Entity\Role;
//use Drupal\KernelTests\KernelTestBase;
// Mit KernelTestBase gibt es die 'permission denied' message durch prog_open().

/**
 * 
 */
class MyPhpUnitTestCase extends UnitTestCase {
  protected $containerSaved = false;
  
  public static $xxTest;
  
  
    public function __construct($name = null, array $data = [], $dataName = '')
    {
      parent::__construct($name, $data, $dataName);
      $this->backupGlobalsBlacklist[] = 'kernel';
    }

  

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
    
    // Drupal::getContainer() darf nicht verwendet werden, 
    // da tests auch vom command-line laufen können müssen.
    //todo::testen::  - alles nicht klar, also erst mal verzichten 

//    $xx_has = \Drupal::hasContainer();
//    if (self::$xxTest) {
//      $xx_test = self::$xxTest;
//    }
//    if (!\Drupal::hasContainer()) {
//      $xx = 0;
//      self::$xxTest = TRUE;
//    }
//    return;

    $drupal_container = myphpunit_drupal_container_get();
    if ($drupal_container) {
      // provide drupal service container for functional tests
      // save container and restore it later
      if (!\Drupal::hasContainer()) {
        \Drupal::setContainer($drupal_container);
        $this->containerSaved = \Drupal::getContainer();
      } else {
        $this->containerSaved = \Drupal::getContainer();
        \Drupal::setContainer($drupal_container);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function tearDown() {
    // restore container first
    if ($this->containerSaved) {
      \Drupal::setContainer($this->containerSaved);
      $this->containerSaved = false;
    }

    parent::tearDown();
  }
  
  protected function ensureTestUser() {
    $user = user_load_by_name('testuser');
    if (!$user) {
      $request_time = \Drupal::time()->getRequestTime();
      $roles = [DRUPAL_AUTHENTICATED_RID];
      User::create([
        'uid'     => NULL,
        'name'    => 'testuser',
        'pass'    => 'asdf',
        'mail'    => 'testuser@localhost.invalid',
        'status'  => 1,
        'created' => $request_time,
        'roles' => array_combine($roles, $roles),
      ])->save();
      $user = user_load_by_name('testuser');
    }
    return $user;
  }
  
  protected function ensureTestRole() {
    $role = Role::load('testrole');
    if (!$role) {
      Role::create([
        'id' => 'testrole',
        'label' => 'TestRole',
      ])->save();
      $role = user_role_load('testrole');
    }
    return $role;
  }
  
  /**
   * Prevent warning: No tests found in class "Drupal\myphpunit\MyPhpUnitTestCase".
   */
  public function testDummy() {
    $this->assertTrue(true);
  }
  

}
