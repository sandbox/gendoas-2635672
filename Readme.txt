Ensure this:
    - Create directory './text-templates'
    - Grant write permissions for that directory

Note:
    - Class PHPUnit_Text_Template is a copy from phpunit
    - Original class name: 'Text_Template'